---
date: '2'
title: 'AMNS Hospitality'
cover: './demo.jpg'
github: ''
external: 'https://play.google.com/store/apps/details?id=com.trois.amns'
tech:
  - React-Native
  - Styled Components
  - RazorPay
  - Sodexo
---

○ For AM/NS Employees and Visitors to order meals online.     

○ Food Menu by Restaurants and Canteens.

○ Google Analytics, Push Notification, Online Payment.
