---
date: '2019-11-12'
title: 'Knownstores'
github: ''
external: 'https://medium.com/stories-from-upstatement/building-a-headless-mobile-app-cms-from-scratch-bab2d17744d9'
tech:
  - Android Native
  - Paytm SDK
company: 'Trois'
showInProjects: true
---

○ KnownStores is a Mobile B2B /B2C e-commerce Hyper-Local Market place where you can shop online and buy products from your favourite local stores.

https://play.google.com/store/apps/details?id=com.knownstores&hl=en&gl=US
